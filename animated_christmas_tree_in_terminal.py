import time, os

#configuration
treeSize=2
fps=2

def c(colorNo, colorCount):
	colorList=[
	"\033[40m", # 0 => black
	"\033[41m", # 1 => red
	"\033[42m", # 2 => green
	"\033[43m", # 3 => yellow
	"\033[44m", # 4 => blue
	"\033[45m", # 5 => purple
	"\033[46m", # 6 => cyan
	"\033[47m" # 7 => white
	]
	return colorList[colorNo]+"  "*treeSize*colorCount+"\033[m"

n="\n"
i=1
lCL=[1, 3, 4, 5, 6] #lightColorList

while True:
	
	#random light code
	v=lCL[(i+1)%5]
	w=lCL[(i+2)%5]
	x=lCL[(i+3)%5]
	y=lCL[(i+4)%5]
	z=lCL[(i+5)%5]
	
	print(
	(c(7, 21)+n)*treeSize+
	(c(7, 21)+n)*treeSize+
	(c(7, 10)+c(v, 1)+c(7, 10)+n)*treeSize+
	(c(7, 9)+c(v, 3)+c(7, 9)+n)*treeSize+
	(c(7, 10)+c(v, 1)+c(7, 10)+n)*treeSize+
	(c(7, 10)+c(2, 1)+c(7, 10)+n)*treeSize+
	(c(7, 9)+c(2, 3)+c(7, 9)+n)*treeSize+
	(c(7, 9)+c(w, 1)+c(2, 2)+c(7, 9)+n)*treeSize+
	(c(7, 8)+c(2, 2)+c(x, 2)+c(2, 1)+c(7, 8)+n)*treeSize+
	(c(7, 8)+c(2, 4)+c(y, 1)+c(7, 8)+n)*treeSize+
	(c(7, 7)+c(2, 1)+c(z, 1)+c(2, 5)+c(7, 7)+n)*treeSize+
	(c(7, 7)+c(2, 2)+c(v, 2)+c(2, 3)+c(7, 7)+n)*treeSize+
	(c(7, 6)+c(y, 1)+c(2, 4)+c(w, 2)+c(2, 2)+c(7, 6)+n)*treeSize+
	(c(7, 6)+c(2, 1)+c(z, 1)+c(2, 5)+c(x, 2)+c(7, 6)+n)*treeSize+
	(c(7, 5)+c(2, 3)+c(v, 2)+c(2, 6)+c(7, 5)+n)*treeSize+
	(c(7, 5)+c(z, 1)+c(2, 4)+c(w, 2)+c(2, 4)+c(7, 5)+n)*treeSize+
	(c(7, 4)+c(2, 2)+c(v, 2)+c(2, 4)+c(x, 2)+c(2, 3)+c(7, 4)+n)*treeSize+
	(c(7, 4)+c(2, 4)+c(w, 2)+c(2, 4)+c(y, 1)+c(2, 2)+c(7, 4)+n)*treeSize+
	(c(7, 9)+c(0, 3)+c(7, 9)+n)*treeSize+
	(c(7, 9)+c(0, 3)+c(7, 9)+n)*treeSize+
	(c(7, 9)+c(0, 3)+c(7, 9)+n)*treeSize+
	(c(7, 9)+c(0, 3)+c(7, 9)+n)*treeSize+
	(c(7, 9)+c(0, 3)+c(7, 9)+n)*treeSize+
	(c(7, 21)+n)*treeSize+
	(c(7, 21)+n)*treeSize
	)
	time.sleep(1/fps)
	os.system("clear")
	i+=1
